import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_btc_ticker/coin_data.dart';
import 'package:flutter_btc_ticker/services/data_service.dart';

class PriceScreen extends StatefulWidget {
  const PriceScreen({Key? key}) : super(key: key);
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = 'EUR';
  String selectedSymbol = 'BTC';
  String valueInCurrency = '?';
  DataService dataService = DataService();
  late Future jsonData;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    try {
      var data = await dataService.fetchExchangeRateBySymbolAndCurrency(
        symbol: selectedSymbol,
        currency: selectedCurrency,
      );
      print(data);
      setState(() {
        valueInCurrency = data.toString();
      });
    } catch (e) {
      print('PriceScreen.getData() caught an error!\t==> ' + e.toString());
    }
  }

  List<Widget> createCards() {
    var widgetList = <Widget>[];
    for (String symbol in CoinData.cryptoList) {
      selectedSymbol = symbol;
      getData();
      widgetList.add(Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 $selectedSymbol = $valueInCurrency $selectedCurrency',
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ));
    }
    return widgetList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: createCards(),
            ),
          ),
          Container(
            width: 100.0,
            height: 150.0,
            alignment: Alignment.center,
            padding: const EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS
                ? buildIOSPicker()
                : buildAndroidDropdownButton(),
          ),
        ],
      ),
    );
  }

  DropdownButton<String> buildAndroidDropdownButton() {
    return DropdownButton<String>(
      dropdownColor: Colors.lightBlue,
      value: selectedCurrency,
      icon: const Icon(
        Icons.arrow_downward,
        color: Colors.white,
      ),
      elevation: 16,
      style: const TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
        fontSize: 25.0,
      ),
      underline: Container(
        height: 3,
        color: Colors.white,
      ),
      onChanged: (String? newValue) {
        setState(() {
          selectedCurrency = newValue!;
        });
        getData();
      },
      items: CoinData.currenciesList
          .map((String currency) =>
              DropdownMenuItem<String>(value: currency, child: Text(currency)))
          .toList(),
    );
  }

  CupertinoPicker buildIOSPicker() {
    var widgetList = <Widget>[];
    for (String currency in CoinData.currenciesList) {
      widgetList.add(Text(currency));
    }

    return CupertinoPicker(
      itemExtent: 35.0,
      onSelectedItemChanged: (int index) {
        setState(() {
          selectedCurrency = CoinData.currenciesList[index];
        });
        getData();
      },
      children: widgetList,
    );
  }
}
