import 'dart:convert' as convert;
import 'dart:io';

import 'package:http/http.dart' as http;

class DataService {
  static const String kCoinApiHost = 'rest.coinapi.io';
  static const String kCoinApiExchangeRatePath = '/v1/exchangerate';
  static const String kCoinApiKey = '86DC9F86-D2A9-4804-BD38-A8F8E401D395';

  Future<dynamic> fetchExchangeRateBySymbolAndCurrency({
    required String symbol,
    required String currency,
  }) async {
    symbol = symbol.toUpperCase();
    currency = currency.toUpperCase();
    String fullPath = kCoinApiExchangeRatePath + "/$symbol/$currency";
    var uri = Uri.https(kCoinApiHost, fullPath, {'apikey': kCoinApiKey});
    var response = await http.get(uri);
    if (response.statusCode == 200) {
      var jsonBody = convert.jsonDecode(response.body);
      return jsonBody['rate'];
    } else {
      return convert.jsonDecode(
          '{"ERROR": "http status is not 200  (= ${response.statusCode})", "msg": ""}');
    }
  }

  Future fetchTest() async {
    var fullUri = Uri.parse(
        'https://$kCoinApiHost$kCoinApiExchangeRatePath/BTC/EUR?apikey=$kCoinApiKey');
    print('DataService.fetchTest()  =>   GET ' + fullUri.toString());
    var response = await http.get(fullUri);
    if (response.statusCode == 200) {
      var json = convert.jsonDecode(response.body);
      var price = json['rate'];
      return price;
    } else {
      throw HttpException("HTTP response code ${response.statusCode} != 200");
    }
  }
}
